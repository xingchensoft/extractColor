﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace demo_win_extractColor
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        ExtractImageColor extract1 = new ExtractImageColor();

        string FILEPATH = "";

        private void btnColor1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FILEPATH))
            {
                MessageBox.Show("请先选择图片文件!");
                return;
            }

            var bitmap = new Bitmap(FILEPATH);
            var list = extract1.ParseColor(bitmap);
            if (bitmap != null) bitmap.Dispose();

            ShowColorPanel(list);
        }

        
        /// <summary>
        /// 显示颜色
        /// </summary>
        /// <param name="list"></param>
        private void ShowColorPanel(Dictionary<Color, float> list)
        {
            panel1.Controls.Clear();
            panel2.Controls.Clear();

            var height = 0;
            for (int i = 0; i < list.Count() && i < 10; i++)
            {
                if (i == 0)
                {
                    Label l = new Label();
                    l.Text = "主色调";
                    l.Location = new Point(0, height);
                    panel1.Controls.Add(l);

                    l = new Label();
                    l.Text = "";
                    l.Location = new Point(0, height);
                    panel2.Controls.Add(l);
                    height += l.Height + 5;
                }
                if (i == 1)
                {
                    Label l = new Label();
                    l.Text = "其他色调";
                    l.Location = new Point(0, height);
                    panel1.Controls.Add(l);

                    l = new Label();
                    l.Text = "";
                    l.Location = new Point(0, height);
                    panel2.Controls.Add(l);
                    height += l.Height + 5;
                }

                {
                    TextBox l = new TextBox();
                    l.BackColor = list.ElementAt(i).Key;
                    l.Location = new Point(0, height);
                    panel1.Controls.Add(l);

                    l = new TextBox();
                    l.Text = string.Format("{0},{1},{2}",
                        list.ElementAt(i).Key.R,
                        list.ElementAt(i).Key.G,
                        list.ElementAt(i).Key.B);
                    l.Location = new Point(0, height);
                    panel2.Controls.Add(l);
                    height += l.Height + 5;
                }

            }
        }

        private void btnColor2_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(FILEPATH))
            {
                MessageBox.Show("请先选择图片文件!");
                return;
            }

            var bitmap = new Bitmap(FILEPATH);
            var list = ExtractImageColor2.getPalette(bitmap);
            if (bitmap != null) bitmap.Dispose();

            Dictionary<Color, float> dis = new Dictionary<Color, float>();
            list.Foreach((o)=> {
                dis.Add(Color.FromArgb(o[0].Value, o[1].Value, o[2].Value), 0);
            });
            ShowColorPanel(dis);
        }

        private void btnFileSelect_Click(object sender, EventArgs e)
        {
            var openFileWindow = new OpenFileDialog();
            if(openFileWindow.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.BackgroundImage = new Bitmap(openFileWindow.FileName);
                FILEPATH = openFileWindow.FileName;
            }
        }

        
    }
}
